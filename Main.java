package org.sample;

import java.util.Scanner;

public class Main {
    public static int COUNT_OF_THREADS = 4;
    public static int COUNT_OF_NUMBERS;
    public static int SECTION;
    public static volatile Carry[] carryTransfer;
    static volatile Carry[] carries;
    static volatile int cntr;

    public static void main(String[] args)  {
        Scanner scan = new Scanner(System.in);
        String A = scan.nextLine();
        String B = scan.nextLine();

        //Set SECTION
        COUNT_OF_NUMBERS = Math.max(A.length(), B.length()) + 1;
        SECTION = COUNT_OF_NUMBERS / COUNT_OF_THREADS;

        //init array bigIntA and bigIntB
        int[] bigIntA = new int[COUNT_OF_NUMBERS];
        int[] bigIntB = new int[COUNT_OF_NUMBERS];
        for (int i = 0; i < COUNT_OF_NUMBERS; i++) {
            bigIntA[i] = 0;
            bigIntB[i] = 0;
        }

        //Scan line and reverse result - fix
        for (int i = 0; i < A.length(); i++) {
            bigIntA[COUNT_OF_NUMBERS - i - 1] = A.charAt(A.length() - i - 1) - '0';
        }
        bigIntA = reverseList(bigIntA);
        for (int i = 0; i < B.length(); i++) {
            bigIntB[COUNT_OF_NUMBERS - i - 1] = B.charAt(B.length() - i - 1) - '0';
        }
        bigIntB = reverseList(bigIntB);

        //Compute and print
        try {
            printBigInt(singleAlgSum(bigIntA, bigIntB));
            printBigInt(multiAlgSum(bigIntA, bigIntB));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static int[] multiAlgSum(int[] bigIntA, int[] bigIntB) throws InterruptedException {
        if (COUNT_OF_NUMBERS < COUNT_OF_THREADS) {
            return singleAlgSum(bigIntA, bigIntB);
        }
        Object synchron = new Object();
        Thread[] threads = new Thread[COUNT_OF_THREADS];
        carries = new Carry[COUNT_OF_NUMBERS];
        carryTransfer = new Carry[COUNT_OF_THREADS];
        int[] result = new int[COUNT_OF_NUMBERS];
        
        for (int i = 0; i < COUNT_OF_THREADS; i++) {
            if (i == COUNT_OF_THREADS - 1) {
                threads[i] = new Thread(new ParallelSummator(i * SECTION, COUNT_OF_NUMBERS, bigIntA, bigIntB, carries, result, synchron));
            } else {
                threads[i] = new Thread(new ParallelSummator(i * SECTION, SECTION * (i + 1), bigIntA, bigIntB, carries, result, synchron));
            }
            threads[i].start();
        }
        for (int i = 0; i < COUNT_OF_THREADS; i++) {
            threads[i].join();
        }
        return result;
    }

    //For testing
    public static int[] multiAlgSumForTest(int[] bigIntA, int[] bigIntB, int COUNT_OF_THREADS, int COUNT_OF_NUMBERS) throws InterruptedException {
        int SECTION = COUNT_OF_NUMBERS / COUNT_OF_THREADS;
        if (COUNT_OF_NUMBERS < COUNT_OF_THREADS) {
            return singleAlgSum(bigIntA, bigIntB);
        }
        Object synchron = new Object();
        Thread[] threads = new Thread[COUNT_OF_THREADS];
        carries = new Carry[COUNT_OF_NUMBERS];
        carryTransfer = new Carry[COUNT_OF_THREADS];
        int[] result = new int[COUNT_OF_NUMBERS];

        for (int i = 0; i < COUNT_OF_THREADS; i++) {
            if (i == COUNT_OF_THREADS - 1) {
                threads[i] = new Thread(new ParallelSummator(i * SECTION, COUNT_OF_NUMBERS, bigIntA, bigIntB, carries, result, synchron));
            } else {
                threads[i] = new Thread(new ParallelSummator(i * SECTION, SECTION * (i + 1), bigIntA, bigIntB, carries, result, synchron));
            }
            threads[i].start();
        }
        for (int i = 0; i < COUNT_OF_THREADS; i++) {
            threads[i].join();
        }
        return result;
    }


    public static int[] singleAlgSum(int[] A, int[] B) {
        int carry = 0;
        int[] sum = new int[COUNT_OF_NUMBERS];
        for (int i = 0; i < COUNT_OF_NUMBERS; i++) {
            sum[i] = (A[i] + B[i] + carry) % 10;
            carry = (A[i] + B[i] + carry) / 10;
        }
        return sum;
    }

    //print array num in right order
    private static void printBigInt(int[] A) {
        boolean flag = false;
        for (int i = A.length - 1; i >= 0; i--) {
            if (flag) {
                System.out.print(A[i]);
            } else if (A[i] != 0 && !flag) {
                flag = true;
                System.out.print(A[i]);
            }
        }
        System.out.println();
    }


    public static int[] reverseList(int[] A) {
        int buff;
        for (int i = 0; i < ((COUNT_OF_NUMBERS % 2 == 0) ? COUNT_OF_NUMBERS / 2 : COUNT_OF_NUMBERS / 2 + 1); i++) {
            buff = A[i];
            A[i] = A[COUNT_OF_NUMBERS - i - 1];
            A[COUNT_OF_NUMBERS - i - 1] = buff;
        }
        return A;
    }
}