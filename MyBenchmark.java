package org.sample;

import org.openjdk.jmh.annotations.*;
import java.util.concurrent.TimeUnit;

public class MyBenchmark {

    @State(Scope.Thread)
    public static class State1 {
        @Setup(Level.Trial)
        public void doSetup() {
            bigIntA = new int[COUNT_OF_NUMBERS];
            bigIntB = new int[COUNT_OF_NUMBERS];

            for (int i = 0; i < COUNT_OF_NUMBERS; i++) {
                bigIntA[i] = 9;
                bigIntB[i] = 5;
            }
            bigIntA = Main.reverseList(bigIntA);
            bigIntB = Main.reverseList(bigIntB);
        }
        int[] bigIntA;
        int[] bigIntB;
        int COUNT_OF_NUMBERS = 100000000;
    }
    @Benchmark @Fork(1) @Warmup(iterations = 5) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void parallel_1_thread(State1 state) {
        try {
            Main.multiAlgSumForTest(state.bigIntA, state.bigIntB, 1, state.COUNT_OF_NUMBERS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Benchmark @Fork(1) @Warmup(iterations = 5) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void parallel_2_threads(State1 state) {
        try {
            Main.multiAlgSumForTest(state.bigIntA, state.bigIntB, 2, state.COUNT_OF_NUMBERS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Benchmark @Fork(1) @Warmup(iterations = 5) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void parallel_4_threads(State1 state) {
        try {
            Main.multiAlgSumForTest(state.bigIntA, state.bigIntB, 4, state.COUNT_OF_NUMBERS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Benchmark @Fork(1) @Warmup(iterations = 5) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void parallel_8_threads(State1 state) {
        try {
            Main.multiAlgSumForTest(state.bigIntA, state.bigIntB, 8, state.COUNT_OF_NUMBERS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Benchmark @Fork(1) @Warmup(iterations = 5) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void single_thread_impl(State1 state) {
        Main.singleAlgSum(state.bigIntA, state.bigIntB);
    }
}
