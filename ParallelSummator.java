package org.sample;

public class ParallelSummator implements Runnable {
    private int start;
    private int end;
    private int[] bigIntA;
    private int[] bigIntB;
    private Carry[] carries;
    private int[] result;
    private Object synchron;
    private Carry carryTransfer;



    public ParallelSummator(int start, int end, int[] bigIntA, int[] bigIntB, Carry[] carries, int[] result, Object synchron) {
        this.start = start;
        this.end = end;
        this.bigIntA = bigIntA;
        this.bigIntB = bigIntB;
        this.carries = carries;
        this.result = result;
        this.synchron = synchron;
        carryTransfer = Carry.M;
    }
    public static Carry operator(Carry a, Carry b) {
        if (b != Carry.M) {
            return b;
        } else {
            return a;
        }
    }
    private void waitEnd() {
        synchronized (synchron) {
            Main.cntr++;
            if (Main.cntr == Main.COUNT_OF_THREADS) {
                Main.cntr = 0;
                synchron.notifyAll();
            } else {
                try {
                    synchron.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public void run() {
        int sum = 0;
        Carry resultTreadCarry;
        for (int i = start; i < end; i++) {
            sum = bigIntA[i] + bigIntB[i];
            if (sum < 9) {
                carries[i] = Carry.N;
            } else if (sum == 9) {
                carries[i] = Carry.M;
            } else {
                carries[i] = Carry.C;
            }
            carryTransfer = operator(carryTransfer, carries[i]);
        }
        Main.carryTransfer[start/Main.SECTION] = carryTransfer;
        waitEnd();
        if (start == 0) {
            PrefixScan hd = new PrefixScan(1, Main.COUNT_OF_THREADS);
            hd.buildTree(Main.carryTransfer, 1, Main.COUNT_OF_THREADS);
            hd.up();
            hd.down(Carry.M);
        }
        waitEnd();
        if (start != 0) {
            resultTreadCarry = Main.carryTransfer[start / Main.SECTION];
        } else {
            resultTreadCarry = Carry.N;
        }
        carries[start] = operator(resultTreadCarry, carries[start]);
        for (int i = start + 1; i < end; i++) {
            carries[i] = operator(carries[i - 1], carries[i]);
        }
        waitEnd();
        for (int i = start; i < end; i++) {
            if (i != 0) {
                result[i] = (bigIntA[i] + bigIntB[i] + (Main.carries[i - 1] == Carry.C ? 1 : 0)) % 10;
            } else {
                result[i] = (bigIntA[i] + bigIntB[i]) % 10;
            }
        }
    }
}