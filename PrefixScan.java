package org.sample;

public class PrefixScan {
    private PrefixScan top;
    private PrefixScan bot;
    private Carry val;
    private int start;
    private int end;

    public PrefixScan(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public Carry getVal() {
        return val;
    }

    public void buildTree(Carry[] arr, int start, int end) {
        if (start == end) {
            this.val = arr[start - 1];
        } else {
            int mid = start + (end - start + 1) / 2 - 1;
            top = new PrefixScan(start, mid);
            bot = new PrefixScan(mid + 1, end);
            Thread thread = new Thread(() -> top.buildTree(arr, start, mid));
            thread.start();
            bot.buildTree(arr, mid + 1, end);
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Carry up() {
        if (val != null) {
            return val;
        } else {
            Up runTop = new Up(top);
            Up runBot = new Up(bot);
            Thread thread = new Thread(runTop);
            thread.start();
            runBot.run();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            val = ParallelSummator.operator(runTop.getVal(), runBot.getVal());
            return val;
        }
    }

    public void down(Carry carry) {
        val = carry;
        if (start == end) {
            Main.carryTransfer[start - 1] = val;
            return;
        }
        Carry newVal = ParallelSummator.operator(val, top.getVal());
        Thread thread = new Thread(() -> top.down(val));
        thread.start();
        bot.down(newVal);
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


class Up implements Runnable {
    private PrefixScan prefixScan;
    private Carry value;
    public Up(PrefixScan prefixScan) {
        this.prefixScan = prefixScan;
    }
    @Override
    public void run() {
        value = prefixScan.up();
    }
    public Carry getVal() {
        return value;
    }
}